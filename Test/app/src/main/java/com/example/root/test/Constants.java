package com.example.root.test;

public final class Constants {
    public static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";

    public static final String BASE_ICON_URL = "http://openweathermap.org/img/w/";
    public static final String ICON_EXTENSION = ".png";
}